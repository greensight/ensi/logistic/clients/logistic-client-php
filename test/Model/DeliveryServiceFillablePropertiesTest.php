<?php
/**
 * DeliveryServiceFillablePropertiesTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\LogisticClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Logistic
 *
 * Управление логистикой
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\LogisticClient;

use PHPUnit\Framework\TestCase;

/**
 * DeliveryServiceFillablePropertiesTest Class Doc Comment
 *
 * @category    Class
 * @description DeliveryServiceFillableProperties
 * @package     Ensi\LogisticClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class DeliveryServiceFillablePropertiesTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "DeliveryServiceFillableProperties"
     */
    public function testDeliveryServiceFillableProperties()
    {
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
    }

    /**
     * Test attribute "registered_at"
     */
    public function testPropertyRegisteredAt()
    {
    }

    /**
     * Test attribute "legal_info_company_name"
     */
    public function testPropertyLegalInfoCompanyName()
    {
    }

    /**
     * Test attribute "legal_info_company_address"
     */
    public function testPropertyLegalInfoCompanyAddress()
    {
    }

    /**
     * Test attribute "legal_info_inn"
     */
    public function testPropertyLegalInfoInn()
    {
    }

    /**
     * Test attribute "legal_info_payment_account"
     */
    public function testPropertyLegalInfoPaymentAccount()
    {
    }

    /**
     * Test attribute "legal_info_bik"
     */
    public function testPropertyLegalInfoBik()
    {
    }

    /**
     * Test attribute "legal_info_bank"
     */
    public function testPropertyLegalInfoBank()
    {
    }

    /**
     * Test attribute "legal_info_bank_correspondent_account"
     */
    public function testPropertyLegalInfoBankCorrespondentAccount()
    {
    }

    /**
     * Test attribute "general_manager_name"
     */
    public function testPropertyGeneralManagerName()
    {
    }

    /**
     * Test attribute "contract_number"
     */
    public function testPropertyContractNumber()
    {
    }

    /**
     * Test attribute "contract_date"
     */
    public function testPropertyContractDate()
    {
    }

    /**
     * Test attribute "vat_rate"
     */
    public function testPropertyVatRate()
    {
    }

    /**
     * Test attribute "taxation_type"
     */
    public function testPropertyTaxationType()
    {
    }

    /**
     * Test attribute "status"
     */
    public function testPropertyStatus()
    {
    }

    /**
     * Test attribute "comment"
     */
    public function testPropertyComment()
    {
    }

    /**
     * Test attribute "apiship_key"
     */
    public function testPropertyApishipKey()
    {
    }

    /**
     * Test attribute "priority"
     */
    public function testPropertyPriority()
    {
    }

    /**
     * Test attribute "max_shipments_per_day"
     */
    public function testPropertyMaxShipmentsPerDay()
    {
    }

    /**
     * Test attribute "max_cargo_export_time"
     */
    public function testPropertyMaxCargoExportTime()
    {
    }

    /**
     * Test attribute "do_consolidation"
     */
    public function testPropertyDoConsolidation()
    {
    }

    /**
     * Test attribute "do_deconsolidation"
     */
    public function testPropertyDoDeconsolidation()
    {
    }

    /**
     * Test attribute "do_zero_mile"
     */
    public function testPropertyDoZeroMile()
    {
    }

    /**
     * Test attribute "do_express_delivery"
     */
    public function testPropertyDoExpressDelivery()
    {
    }

    /**
     * Test attribute "do_return"
     */
    public function testPropertyDoReturn()
    {
    }

    /**
     * Test attribute "do_dangerous_products_delivery"
     */
    public function testPropertyDoDangerousProductsDelivery()
    {
    }

    /**
     * Test attribute "do_transportation_oversized_cargo"
     */
    public function testPropertyDoTransportationOversizedCargo()
    {
    }

    /**
     * Test attribute "add_partial_reject_service"
     */
    public function testPropertyAddPartialRejectService()
    {
    }

    /**
     * Test attribute "add_insurance_service"
     */
    public function testPropertyAddInsuranceService()
    {
    }

    /**
     * Test attribute "add_fitting_service"
     */
    public function testPropertyAddFittingService()
    {
    }

    /**
     * Test attribute "add_return_service"
     */
    public function testPropertyAddReturnService()
    {
    }

    /**
     * Test attribute "add_open_service"
     */
    public function testPropertyAddOpenService()
    {
    }

    /**
     * Test attribute "pct"
     */
    public function testPropertyPct()
    {
    }
}
