<?php
/**
 * DeliveryServiceEnum
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\LogisticClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Logistic
 *
 * Управление логистикой
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\LogisticClient\Dto;
use \Ensi\LogisticClient\ObjectSerializer;

/**
 * DeliveryServiceEnum Class Doc Comment
 *
 * @category Class
 * @description Службы доставки. Расшифровка значений:   * &#x60;1&#x60; - TK1   * &#x60;2&#x60; - TK2   * &#x60;3&#x60; - TK3   * &#x60;4&#x60; - TK4   * &#x60;5&#x60; - TK5   * &#x60;6&#x60; - TK6   * &#x60;7&#x60; - TK7   * &#x60;8&#x60; - TK8   * &#x60;9&#x60; - TK9   * &#x60;10&#x60;- TK10
 * @package  Ensi\LogisticClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class DeliveryServiceEnum
{
    
    /** TK1 */
    public const TK1 = 1;
    
    /** TK2 */
    public const TK2 = 2;
    
    /** TK3 */
    public const TK3 = 3;
    
    /** TK4 */
    public const TK4 = 4;
    
    /** TK5 */
    public const TK5 = 5;
    
    /** TK6 */
    public const TK6 = 6;
    
    /** TK7 */
    public const TK7 = 7;
    
    /** TK8 */
    public const TK8 = 8;
    
    /** TK9 */
    public const TK9 = 9;
    
    /** TK10 */
    public const TK10 = 10;
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues(): array
    {
        return [
            self::TK1,
            self::TK2,
            self::TK3,
            self::TK4,
            self::TK5,
            self::TK6,
            self::TK7,
            self::TK8,
            self::TK9,
            self::TK10,
        ];
    }

    /**
    * Gets allowable values and titles of the enum
    * @return string[]
    */
    public static function getDescriptions(): array
    {
        return [
            self::TK1 => 'TK1',
            self::TK2 => 'TK2',
            self::TK3 => 'TK3',
            self::TK4 => 'TK4',
            self::TK5 => 'TK5',
            self::TK6 => 'TK6',
            self::TK7 => 'TK7',
            self::TK8 => 'TK8',
            self::TK9 => 'TK9',
            self::TK10 => 'TK10',
        ];
    }
}


