# # CargoIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipment_links** | [**\Ensi\LogisticClient\Dto\CargoShipmentLink[]**](CargoShipmentLink.md) | Ссылки на отправления в OMS | [optional] 
**delivery_service** | [**\Ensi\LogisticClient\Dto\DeliveryService**](DeliveryService.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


