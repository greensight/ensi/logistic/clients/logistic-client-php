# # CargoReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id | 
**seller_id** | **int** | Идентификатор продавца | 
**store_id** | **int** | Идентификатор склада продавца | 
**delivery_service_id** | **int** | Идентификатор сервиса доставки | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**status_at** | [**\DateTime**](\DateTime.md) | Дата установки статуса | 
**is_problem_at** | [**\DateTime**](\DateTime.md) | Дата установки флага проблемного груза | 
**is_canceled_at** | [**\DateTime**](\DateTime.md) | Дата установки флага отмены груза | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


