# # DeliveryOrderPlace

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор места в заказе на доставку | 
**delivery_order_id** | **int** | Id заказа на доставку | 
**number** | **string** | Номер места в информационной системе клиента | 
**barcode** | **string** | Штрихкод места | 
**height** | **int** | Высота места (в мм) | 
**length** | **int** | Длина места (в мм) | 
**width** | **int** | Ширина места (в мм) | 
**weight** | **int** | Вес места (в граммах) | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**items** | [**\Ensi\LogisticClient\Dto\DeliveryOrderPlaceItem[]**](DeliveryOrderPlaceItem.md) | Товары места | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


