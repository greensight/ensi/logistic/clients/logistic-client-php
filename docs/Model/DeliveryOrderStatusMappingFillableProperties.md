# # DeliveryOrderStatusMappingFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delivery_service_id** | **int** |  | [optional] 
**delivery_service** | [**\Ensi\LogisticClient\Dto\DeliveryService**](DeliveryService.md) |  | [optional] 
**status** | **int** |  | [optional] 
**external_status** | **string** | Статус заказа на доставку у службы доставки (зависит от службы доставки) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


