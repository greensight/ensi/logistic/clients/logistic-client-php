# # CargoOrderReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id | 
**cdek_intake_number** | **string** | Номер заявки СДЭК на вызов курьера | 
**external_id** | **string** | Номер заявки во внешней системе | 
**error_external_id** | **string** | Текст последней ошибки при создании заявки на вызов курьера для забора груза в службе доставки | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


