# # PointFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delivery_service_id** | **int** | Идентификатор службы доставки из DeliveryServiceEnum | [optional] 
**is_active** | **bool** | Активность | [optional] 
**address** | [**\Ensi\LogisticClient\Dto\Address**](Address.md) |  | [optional] 
**city_guid** | **string** | ФИАС id населенного пункта | [optional] 
**geo_lat** | **string** | Широта | [optional] 
**geo_lon** | **string** | Долгота | [optional] 
**timezone** | **string** | Часовой пояс | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


