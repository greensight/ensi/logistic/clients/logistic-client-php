# # Region

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор региона | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**federal_district_id** | **int** | Id федерального округа | [optional] 
**name** | **string** | Название | [optional] 
**guid** | **string** | Id ФИАС | [optional] 
**cities** | [**\Ensi\LogisticClient\Dto\City[]**](City.md) | Населенные пункты, в которые возможна доставка | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


