# # DeliveryServiceManagerIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delivery_service** | [**\Ensi\LogisticClient\Dto\DeliveryService**](DeliveryService.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


