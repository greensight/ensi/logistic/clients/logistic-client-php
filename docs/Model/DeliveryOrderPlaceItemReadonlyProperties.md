# # DeliveryOrderPlaceItemReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор товара места (коробки) в заказе на доставку | 
**delivery_order_place_id** | **int** | Идентификатор места (коробки) в заказе на доставку | 
**vendor_code** | **string** | Артикул товара | 
**barcode** | **string** | Штрихкод на товаре | 
**name** | **string** | Наименование товара | 
**qty** | **float** | Кол-во товара | 
**qty_delivered** | **float** | Заполняется только при частичной доставке и показывает сколько вложимых выкуплено | 
**height** | **int** | Высота единицы товара (в мм) | 
**length** | **int** | Длина единицы товара (в мм) | 
**width** | **int** | Ширина единицы товара (в мм) | 
**weight** | **int** | Вес единицы товара (в граммах) | 
**assessed_cost** | **int** | Оценочная стоимость единицы товара (в копейках) | 
**cost** | **int** | Стоимость единицы товара с учетом НДС (в копейках) | 
**cost_vat** | **int** | Процентная ставка НДС | 
**price** | **int** | Цена единицы товара к оплате с учетом НДС (в копейках) | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


