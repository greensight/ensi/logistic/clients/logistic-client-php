# # DeliveryServiceDocument

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор документа | 
**file** | [**\Ensi\LogisticClient\Dto\FileOrNull**](FileOrNull.md) |  | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**delivery_service_id** | **int** | Id службы доставки | [optional] 
**name** | **string** | Название | [optional] 
**delivery_service** | [**\Ensi\LogisticClient\Dto\DeliveryService**](DeliveryService.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


