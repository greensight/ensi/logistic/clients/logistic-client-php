# # DeliveryKpi

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**rtg** | **int** | Ready-To-Go time - время для проверки заказа АОЗ до его передачи в MAS (мин) | [optional] 
**ct** | **int** | Confirmation Time - время перехода Отправления из статуса “Ожидает подтверждения” в статус “На комплектации” (мин) | [optional] 
**ppt** | **int** | Planned Processing Time - плановое время для прохождения Отправлением статусов от “На комплектации” до “Готов к передаче ЛО” (мин) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


