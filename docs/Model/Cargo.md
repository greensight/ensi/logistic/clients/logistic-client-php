# # Cargo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id | 
**seller_id** | **int** | Идентификатор продавца | 
**store_id** | **int** | Идентификатор склада продавца | 
**delivery_service_id** | **int** | Идентификатор сервиса доставки | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**status_at** | [**\DateTime**](\DateTime.md) | Дата установки статуса | 
**is_problem_at** | [**\DateTime**](\DateTime.md) | Дата установки флага проблемного груза | 
**is_canceled_at** | [**\DateTime**](\DateTime.md) | Дата установки флага отмены груза | 
**status** | **int** |  | [optional] 
**is_problem** | **bool** | Флаг, что у груза проблемы при отгрузке | [optional] 
**is_canceled** | **bool** | Флаг, что груз отменен | [optional] 
**width** | **float** | Ширина | [optional] 
**height** | **float** | Высота | [optional] 
**length** | **float** | Длина | [optional] 
**weight** | **float** | Вес | [optional] 
**shipping_problem_comment** | **string** | Последнее сообщение мерчанта о проблеме с отгрузкой | [optional] 
**shipment_links** | [**\Ensi\LogisticClient\Dto\CargoShipmentLink[]**](CargoShipmentLink.md) | Ссылки на отправления в OMS | [optional] 
**delivery_service** | [**\Ensi\LogisticClient\Dto\DeliveryService**](DeliveryService.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


