# # DeliveryService

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор службы доставки | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**name** | **string** | Название | [optional] 
**registered_at** | [**\DateTime**](\DateTime.md) | Дата регистрации | [optional] 
**legal_info_company_name** | **string** | Юр. название компании | [optional] 
**legal_info_company_address** | [**\Ensi\LogisticClient\Dto\Address**](Address.md) |  | [optional] 
**legal_info_inn** | **string** | ИНН | [optional] 
**legal_info_payment_account** | **string** | р/с | [optional] 
**legal_info_bik** | **string** | БИК | [optional] 
**legal_info_bank** | **string** | Банк | [optional] 
**legal_info_bank_correspondent_account** | **string** | К/с банка | [optional] 
**general_manager_name** | **string** | ФИО ген. директора | [optional] 
**contract_number** | **string** | Номер договора | [optional] 
**contract_date** | [**\DateTime**](\DateTime.md) | Дата договора | [optional] 
**vat_rate** | **int** | Cтавка НДС | [optional] 
**taxation_type** | **int** | Тип налогообложения | [optional] 
**status** | **int** |  | [optional] 
**comment** | **string** | Комментарий | [optional] 
**apiship_key** | **string** | Код в системе ApiShip | [optional] 
**priority** | **int** | Приоритет (чем меньше, тем выше) | [optional] 
**max_shipments_per_day** | **int** | Максимальное кол-во отправлений в день | [optional] 
**max_cargo_export_time** | **string** | Время создания заявки для забора отправления день-в-день | [optional] 
**do_consolidation** | **bool** | Консолидация многоместных отправлений? | [optional] 
**do_deconsolidation** | **bool** | Расконсолидация многоместных отправлений? | [optional] 
**do_zero_mile** | **bool** | Осуществляет нулевую милю? | [optional] 
**do_express_delivery** | **bool** | Осуществляет экспресс-доставку? | [optional] 
**do_return** | **bool** | Принимает возвраты? | [optional] 
**do_dangerous_products_delivery** | **bool** | Осуществляет доставку опасных грузов? | [optional] 
**do_transportation_oversized_cargo** | **bool** | Перевозка крупногабаритных грузов? | [optional] 
**add_partial_reject_service** | **bool** | Добавлять услугу частичного отказ в заказ на доставку? | [optional] 
**add_insurance_service** | **bool** | Добавлять услугу страхования груза в заказ на доставку? | [optional] 
**add_fitting_service** | **bool** | Добавлять услугу примерки в заказ на доставку? | [optional] 
**add_return_service** | **bool** | Добавлять услугу возврата в заказ на доставку? | [optional] 
**add_open_service** | **bool** | Добавлять услугу вскрытия при получении в заказ на доставку? | [optional] 
**pct** | **int** | Planned Consolidation Time - плановое время доставки заказа от склада продавца до логистического хаба ЛО и обработки заказа в сортировочном центре или хабе на стороне ЛО (мин) | [optional] 
**payment_methods** | **int[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


