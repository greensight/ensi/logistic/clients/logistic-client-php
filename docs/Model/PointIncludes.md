# # PointIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**point_workings** | [**\Ensi\LogisticClient\Dto\PointWorking[]**](PointWorking.md) | Время работы | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


