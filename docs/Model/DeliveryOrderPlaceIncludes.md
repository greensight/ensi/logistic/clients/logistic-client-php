# # DeliveryOrderPlaceIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**\Ensi\LogisticClient\Dto\DeliveryOrderPlaceItem[]**](DeliveryOrderPlaceItem.md) | Товары места | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


