# # CheckoutDeliveryPriceRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**region_guid** | **string** | Id ФИАС региона | 
**delivery_method** | **int** |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


