# # PointWorking

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор | 
**point_id** | **int** | Идентификатор пункта | 
**is_active** | **bool** | Активность | 
**day** | **int** | День недели (1-7) | 
**working_start_time** | **string** | Время начала работы склада | 
**working_end_time** | **string** | Время конца работы склада | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


