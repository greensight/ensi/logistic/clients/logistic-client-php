# # FederalDistrictIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**regions** | [**\Ensi\LogisticClient\Dto\Region[]**](Region.md) | Регионы | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


