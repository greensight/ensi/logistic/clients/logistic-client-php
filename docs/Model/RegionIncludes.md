# # RegionIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cities** | [**\Ensi\LogisticClient\Dto\City[]**](City.md) | Населенные пункты, в которые возможна доставка | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


