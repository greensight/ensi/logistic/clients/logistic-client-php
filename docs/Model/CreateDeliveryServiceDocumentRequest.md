# # CreateDeliveryServiceDocumentRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delivery_service_id** | **int** | Id службы доставки | [optional] 
**name** | **string** | Название | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


