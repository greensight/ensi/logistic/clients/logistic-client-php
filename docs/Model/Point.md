# # Point

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор пункта | 
**external_id** | **string** | Id пункта у службы доставки | 
**name** | **string** | Название пункта | 
**description** | **string** | Описание проезда | 
**phone** | **string** | Телефон | 
**address_reduce** | **string** | Cокращенный адрес пвз | 
**metro_station** | **string** | Cтанция метро | 
**only_online_payment** | **bool** | Выдача только полностью оплаченных посылок | 
**has_payment_card** | **bool** | Возможна ли оплата картой | 
**has_courier** | **bool** | Осуществляется ли курьерская доставка | 
**is_postamat** | **bool** | Отделение является постаматом | 
**max_value** | **string** | Ограничение объема (куб.метры) | 
**max_weight** | **int** | Ограничение веса (кг) | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**delivery_service_id** | **int** | Идентификатор службы доставки из DeliveryServiceEnum | [optional] 
**is_active** | **bool** | Активность | [optional] 
**address** | [**\Ensi\LogisticClient\Dto\Address**](Address.md) |  | [optional] 
**city_guid** | **string** | ФИАС id населенного пункта | [optional] 
**geo_lat** | **string** | Широта | [optional] 
**geo_lon** | **string** | Долгота | [optional] 
**timezone** | **string** | Часовой пояс | [optional] 
**point_workings** | [**\Ensi\LogisticClient\Dto\PointWorking[]**](PointWorking.md) | Время работы | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


