# # DeliveryOrder

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор заказа на доставку | 
**delivery_id** | **int** | Id Отправления в OMS | 
**delivery_service_id** | **int** |  | 
**number** | **string** | Номер заказа на доставку внутри платформы (совпадает с номером Отправления в OMS) | 
**external_id** | **string** | Номер заказа на доставку у службы доставки (не заполняется при создании) | 
**error_external_id** | **string** | Текст последней ошибки при создании заказа на доставку в службе доставки | 
**status_at** | [**\DateTime**](\DateTime.md) | Дата и время изменения статуса заказа на доставку внутри платформы | 
**external_status** | **string** | Статус заказа на доставку у службы доставки | 
**external_status_at** | [**\DateTime**](\DateTime.md) | Дата и время изменения статуса заказа на доставку у службы доставки | 
**height** | **int** | Высота заказа (в мм) | 
**length** | **int** | Длина заказа (в мм) | 
**width** | **int** | Ширина заказа (в мм) | 
**weight** | **int** | Вес заказа (в граммах) | 
**shipment_method** | **int** |  | 
**delivery_method** | **int** |  | 
**delivery_date** | [**\DateTime**](\DateTime.md) | Желаемая дата доставки | 
**point_in_id** | **int** | Id точки приема заказа | 
**point_out_id** | **int** | Id точки выдачи заказа | 
**shipment_time_start** | [**\DateTime**](\DateTime.md) | Начальное время забора от продавца | 
**shipment_time_end** | [**\DateTime**](\DateTime.md) | Конечное время забора от продавца | 
**delivery_time_start** | [**\DateTime**](\DateTime.md) | Начальное время доставки | 
**delivery_time_end** | [**\DateTime**](\DateTime.md) | Конечное время доставки | 
**description** | **string** | Комментарий | 
**assessed_cost** | **int** | Оценочная стоимость / сумма страховки (в копейках) | 
**delivery_cost** | **int** | Стоимость доставки с учетом НДС (в копейках) | 
**delivery_cost_vat** | **float** | Процентная ставка НДС | 
**delivery_cost_pay** | **int** | Стоимость доставки к оплате с учетом НДС (в копейках) | 
**cod_cost** | **int** | Сумма наложенного платежа с учетом НДС (в копейках) | 
**is_delivery_payed_by_recipient** | **bool** | Флаг для указания стороны, которая платит за услуги доставки (0-отправитель, 1-получатель) | 
**sender_is_seller** | **bool** | Отправитель является истинным продавцом, а не маркетплейсом? | 
**sender_inn** | **string** | ИНН отправителя | 
**sender_address** | [**\Ensi\LogisticClient\Dto\Address**](Address.md) |  | 
**sender_company_name** | **string** | Название компании отправителя | 
**sender_contact_name** | **string** | ФИО контактного лица отправителя | 
**sender_email** | **string** | Контактный email отправителя | 
**sender_phone** | **string** | Контактный email отправителя | 
**sender_comment** | **string** | Комментарий отправителя | 
**recipient_address** | [**\Ensi\LogisticClient\Dto\Address**](Address.md) |  | 
**recipient_company_name** | **string** | Название компании получателя | 
**recipient_contact_name** | **string** | ФИО контактного лица получателя | 
**recipient_email** | **string** | Контактный email получателя | 
**recipient_phone** | **string** | Контактный email получателя | 
**recipient_comment** | **string** | Комментарий получателя | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**status** | **int** |  | [optional] 
**places** | [**\Ensi\LogisticClient\Dto\DeliveryOrderPlace[]**](DeliveryOrderPlace.md) | Места в заказе на доставку | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


