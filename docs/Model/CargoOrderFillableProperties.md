# # CargoOrderFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **int** |  | [optional] 
**cargo_id** | **int** | Идентификатор груза | [optional] 
**timeslot_id** | **string** | Идентификатор таймслота | [optional] 
**timeslot_from** | [**\DateTime**](\DateTime.md) | Дата забора груза | [optional] 
**timeslot_to** | [**\DateTime**](\DateTime.md) | Дата забора груза | [optional] 
**date** | [**\DateTime**](\DateTime.md) | Дата забора груза | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


