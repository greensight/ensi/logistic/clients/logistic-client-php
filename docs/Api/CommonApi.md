# Ensi\LogisticClient\CommonApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkConditionsToDeleteCustomerPersonalData**](CommonApi.md#checkConditionsToDeleteCustomerPersonalData) | **POST** /common/customer-personal-data:check-conditions-to-delete | Checking the conditions in Logistic for deleting the customer&#39;s personal data
[**deleteCustomerPersonalData**](CommonApi.md#deleteCustomerPersonalData) | **POST** /common/customer-personal-data:delete | Deleting customer&#39;s personal data in Logistic



## checkConditionsToDeleteCustomerPersonalData

> \Ensi\LogisticClient\Dto\CheckConditionsToDeleteCustomerPersonalDataResponse checkConditionsToDeleteCustomerPersonalData($check_conditions_to_delete_customer_personal_data_request)

Checking the conditions in Logistic for deleting the customer's personal data

Checking the conditions in Logistic for deleting the customer's personal data

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\CommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$check_conditions_to_delete_customer_personal_data_request = new \Ensi\LogisticClient\Dto\CheckConditionsToDeleteCustomerPersonalDataRequest(); // \Ensi\LogisticClient\Dto\CheckConditionsToDeleteCustomerPersonalDataRequest | 

try {
    $result = $apiInstance->checkConditionsToDeleteCustomerPersonalData($check_conditions_to_delete_customer_personal_data_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->checkConditionsToDeleteCustomerPersonalData: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **check_conditions_to_delete_customer_personal_data_request** | [**\Ensi\LogisticClient\Dto\CheckConditionsToDeleteCustomerPersonalDataRequest**](../Model/CheckConditionsToDeleteCustomerPersonalDataRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\CheckConditionsToDeleteCustomerPersonalDataResponse**](../Model/CheckConditionsToDeleteCustomerPersonalDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteCustomerPersonalData

> \Ensi\LogisticClient\Dto\EmptyDataResponse deleteCustomerPersonalData($delete_customer_personal_data_request)

Deleting customer's personal data in Logistic

Deleting customer's personal data in Logistic

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\CommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$delete_customer_personal_data_request = new \Ensi\LogisticClient\Dto\DeleteCustomerPersonalDataRequest(); // \Ensi\LogisticClient\Dto\DeleteCustomerPersonalDataRequest | 

try {
    $result = $apiInstance->deleteCustomerPersonalData($delete_customer_personal_data_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->deleteCustomerPersonalData: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_customer_personal_data_request** | [**\Ensi\LogisticClient\Dto\DeleteCustomerPersonalDataRequest**](../Model/DeleteCustomerPersonalDataRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

