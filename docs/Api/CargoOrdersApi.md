# Ensi\LogisticClient\CargoOrdersApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cancelCargo**](CargoOrdersApi.md#cancelCargo) | **POST** /cargo-orders/cargo/{id}:cancel | Отмена груза
[**cancelCargoOrder**](CargoOrdersApi.md#cancelCargoOrder) | **POST** /cargo-orders/cargo-orders/{id}:cancel | Отменить заказ на забор груза
[**patchCargo**](CargoOrdersApi.md#patchCargo) | **PATCH** /cargo-orders/cargo/{id} | Обновить данные по грузу
[**searchCargo**](CargoOrdersApi.md#searchCargo) | **POST** /cargo-orders/cargo:search | Поиск грузов
[**searchCargoOrders**](CargoOrdersApi.md#searchCargoOrders) | **POST** /cargo-orders/cargo-orders:search | Поиск заказов на забор груза



## cancelCargo

> \Ensi\LogisticClient\Dto\CargoResponse cancelCargo($id)

Отмена груза

Отмена груза

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\CargoOrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->cancelCargo($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CargoOrdersApi->cancelCargo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\LogisticClient\Dto\CargoResponse**](../Model/CargoResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## cancelCargoOrder

> \Ensi\LogisticClient\Dto\CargoOrderResponse cancelCargoOrder($id)

Отменить заказ на забор груза

Отменить заказ на забор груза

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\CargoOrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->cancelCargoOrder($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CargoOrdersApi->cancelCargoOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\LogisticClient\Dto\CargoOrderResponse**](../Model/CargoOrderResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchCargo

> \Ensi\LogisticClient\Dto\CargoResponse patchCargo($id, $patch_cargo_request)

Обновить данные по грузу

Обновить данные по грузу

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\CargoOrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_cargo_request = new \Ensi\LogisticClient\Dto\PatchCargoRequest(); // \Ensi\LogisticClient\Dto\PatchCargoRequest | 

try {
    $result = $apiInstance->patchCargo($id, $patch_cargo_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CargoOrdersApi->patchCargo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_cargo_request** | [**\Ensi\LogisticClient\Dto\PatchCargoRequest**](../Model/PatchCargoRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\CargoResponse**](../Model/CargoResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchCargo

> \Ensi\LogisticClient\Dto\SearchCargoResponse searchCargo($search_cargo_request)

Поиск грузов

Поиск грузов

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\CargoOrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_cargo_request = new \Ensi\LogisticClient\Dto\SearchCargoRequest(); // \Ensi\LogisticClient\Dto\SearchCargoRequest | 

try {
    $result = $apiInstance->searchCargo($search_cargo_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CargoOrdersApi->searchCargo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_cargo_request** | [**\Ensi\LogisticClient\Dto\SearchCargoRequest**](../Model/SearchCargoRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\SearchCargoResponse**](../Model/SearchCargoResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchCargoOrders

> \Ensi\LogisticClient\Dto\SearchCargoOrdersResponse searchCargoOrders($search_cargo_orders_request)

Поиск заказов на забор груза

Поиск заказов на забор груза

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\CargoOrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_cargo_orders_request = new \Ensi\LogisticClient\Dto\SearchCargoOrdersRequest(); // \Ensi\LogisticClient\Dto\SearchCargoOrdersRequest | 

try {
    $result = $apiInstance->searchCargoOrders($search_cargo_orders_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CargoOrdersApi->searchCargoOrders: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_cargo_orders_request** | [**\Ensi\LogisticClient\Dto\SearchCargoOrdersRequest**](../Model/SearchCargoOrdersRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\SearchCargoOrdersResponse**](../Model/SearchCargoOrdersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

