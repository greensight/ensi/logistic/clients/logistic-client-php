# Ensi\LogisticClient\KpiApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDeliveryKpiCt**](KpiApi.md#createDeliveryKpiCt) | **POST** /delivery-kpis/delivery-kpi-ct/{sellerId} | Создание объекта типа DeliveryKpiCt
[**createDeliveryKpiPpt**](KpiApi.md#createDeliveryKpiPpt) | **POST** /delivery-kpis/delivery-kpi-ppt/{sellerId} | Создание объекта типа DeliveryKpiPpt
[**deleteDeliveryKpiCt**](KpiApi.md#deleteDeliveryKpiCt) | **DELETE** /delivery-kpis/delivery-kpi-ct/{sellerId} | Удаление объекта типа DeliveryKpiCt
[**deleteDeliveryKpiPpt**](KpiApi.md#deleteDeliveryKpiPpt) | **DELETE** /delivery-kpis/delivery-kpi-ppt/{sellerId} | Удаление объекта типа DeliveryKpiPpt
[**getDeliveryKpi**](KpiApi.md#getDeliveryKpi) | **GET** /delivery-kpis/delivery-kpi | Получение объекта типа DeliveryKpi
[**getDeliveryKpiCt**](KpiApi.md#getDeliveryKpiCt) | **GET** /delivery-kpis/delivery-kpi-ct/{sellerId} | Получение объекта типа DeliveryKpiCt
[**getDeliveryKpiPpt**](KpiApi.md#getDeliveryKpiPpt) | **GET** /delivery-kpis/delivery-kpi-ppt/{sellerId} | Получение объекта типа DeliveryKpiPpt
[**patchDeliveryKpi**](KpiApi.md#patchDeliveryKpi) | **PATCH** /delivery-kpis/delivery-kpi | Обновления части полей объекта типа DeliveryKpi
[**patchDeliveryKpiCt**](KpiApi.md#patchDeliveryKpiCt) | **PATCH** /delivery-kpis/delivery-kpi-ct/{sellerId} | Замена объекта типа DeliveryKpiCt
[**patchDeliveryKpiPpt**](KpiApi.md#patchDeliveryKpiPpt) | **PATCH** /delivery-kpis/delivery-kpi-ppt/{sellerId} | Замена объекта типа DeliveryKpiPpt
[**searchDeliveryKpiCts**](KpiApi.md#searchDeliveryKpiCts) | **POST** /delivery-kpis/delivery-kpi-ct:search | Поиск объектов типа DeliveryKpiCt
[**searchDeliveryKpiPpts**](KpiApi.md#searchDeliveryKpiPpts) | **POST** /delivery-kpis/delivery-kpi-ppt:search | Поиск объектов типа DeliveryKpiPpt
[**searchOneDeliveryKpiCt**](KpiApi.md#searchOneDeliveryKpiCt) | **POST** /delivery-kpis/delivery-kpi-ct:search-one | Поиск объекта типа DeliveryKpiCt
[**searchOneDeliveryKpiPpt**](KpiApi.md#searchOneDeliveryKpiPpt) | **POST** /delivery-kpis/delivery-kpi-ppt:search-one | Поиск объекта типа DeliveryKpiPpt



## createDeliveryKpiCt

> \Ensi\LogisticClient\Dto\DeliveryKpiCtResponse createDeliveryKpiCt($seller_id, $create_delivery_kpi_ct_request)

Создание объекта типа DeliveryKpiCt

Создание объекта типа DeliveryKpiCt

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\KpiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$seller_id = 1; // int | Id продавца
$create_delivery_kpi_ct_request = new \Ensi\LogisticClient\Dto\CreateDeliveryKpiCtRequest(); // \Ensi\LogisticClient\Dto\CreateDeliveryKpiCtRequest | 

try {
    $result = $apiInstance->createDeliveryKpiCt($seller_id, $create_delivery_kpi_ct_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KpiApi->createDeliveryKpiCt: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seller_id** | **int**| Id продавца |
 **create_delivery_kpi_ct_request** | [**\Ensi\LogisticClient\Dto\CreateDeliveryKpiCtRequest**](../Model/CreateDeliveryKpiCtRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryKpiCtResponse**](../Model/DeliveryKpiCtResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createDeliveryKpiPpt

> \Ensi\LogisticClient\Dto\DeliveryKpiPptResponse createDeliveryKpiPpt($seller_id, $create_delivery_kpi_ppt_request)

Создание объекта типа DeliveryKpiPpt

Создание объекта типа DeliveryKpiPpt

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\KpiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$seller_id = 1; // int | Id продавца
$create_delivery_kpi_ppt_request = new \Ensi\LogisticClient\Dto\CreateDeliveryKpiPptRequest(); // \Ensi\LogisticClient\Dto\CreateDeliveryKpiPptRequest | 

try {
    $result = $apiInstance->createDeliveryKpiPpt($seller_id, $create_delivery_kpi_ppt_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KpiApi->createDeliveryKpiPpt: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seller_id** | **int**| Id продавца |
 **create_delivery_kpi_ppt_request** | [**\Ensi\LogisticClient\Dto\CreateDeliveryKpiPptRequest**](../Model/CreateDeliveryKpiPptRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryKpiPptResponse**](../Model/DeliveryKpiPptResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteDeliveryKpiCt

> \Ensi\LogisticClient\Dto\EmptyDataResponse deleteDeliveryKpiCt($seller_id)

Удаление объекта типа DeliveryKpiCt

Удаление объекта типа DeliveryKpiCt

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\KpiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$seller_id = 1; // int | Id продавца

try {
    $result = $apiInstance->deleteDeliveryKpiCt($seller_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KpiApi->deleteDeliveryKpiCt: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seller_id** | **int**| Id продавца |

### Return type

[**\Ensi\LogisticClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteDeliveryKpiPpt

> \Ensi\LogisticClient\Dto\EmptyDataResponse deleteDeliveryKpiPpt($seller_id)

Удаление объекта типа DeliveryKpiPpt

Удаление объекта типа DeliveryKpiPpt

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\KpiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$seller_id = 1; // int | Id продавца

try {
    $result = $apiInstance->deleteDeliveryKpiPpt($seller_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KpiApi->deleteDeliveryKpiPpt: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seller_id** | **int**| Id продавца |

### Return type

[**\Ensi\LogisticClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDeliveryKpi

> \Ensi\LogisticClient\Dto\DeliveryKpiResponse getDeliveryKpi()

Получение объекта типа DeliveryKpi

Получение объекта типа DeliveryKpi

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\KpiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getDeliveryKpi();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KpiApi->getDeliveryKpi: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryKpiResponse**](../Model/DeliveryKpiResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDeliveryKpiCt

> \Ensi\LogisticClient\Dto\DeliveryKpiCtResponse getDeliveryKpiCt($seller_id)

Получение объекта типа DeliveryKpiCt

Получение объекта типа DeliveryKpiCt

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\KpiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$seller_id = 1; // int | Id продавца

try {
    $result = $apiInstance->getDeliveryKpiCt($seller_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KpiApi->getDeliveryKpiCt: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seller_id** | **int**| Id продавца |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryKpiCtResponse**](../Model/DeliveryKpiCtResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDeliveryKpiPpt

> \Ensi\LogisticClient\Dto\DeliveryKpiPptResponse getDeliveryKpiPpt($seller_id)

Получение объекта типа DeliveryKpiPpt

Получение объекта типа DeliveryKpiPpt

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\KpiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$seller_id = 1; // int | Id продавца

try {
    $result = $apiInstance->getDeliveryKpiPpt($seller_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KpiApi->getDeliveryKpiPpt: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seller_id** | **int**| Id продавца |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryKpiPptResponse**](../Model/DeliveryKpiPptResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDeliveryKpi

> \Ensi\LogisticClient\Dto\DeliveryKpiResponse patchDeliveryKpi($patch_delivery_kpi_request)

Обновления части полей объекта типа DeliveryKpi

Обновления части полей объекта типа DeliveryKpi

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\KpiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$patch_delivery_kpi_request = new \Ensi\LogisticClient\Dto\PatchDeliveryKpiRequest(); // \Ensi\LogisticClient\Dto\PatchDeliveryKpiRequest | 

try {
    $result = $apiInstance->patchDeliveryKpi($patch_delivery_kpi_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KpiApi->patchDeliveryKpi: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patch_delivery_kpi_request** | [**\Ensi\LogisticClient\Dto\PatchDeliveryKpiRequest**](../Model/PatchDeliveryKpiRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryKpiResponse**](../Model/DeliveryKpiResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDeliveryKpiCt

> \Ensi\LogisticClient\Dto\DeliveryKpiCtResponse patchDeliveryKpiCt($seller_id, $patch_delivery_kpi_ct_request)

Замена объекта типа DeliveryKpiCt

Замена объекта типа DeliveryKpiCt

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\KpiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$seller_id = 1; // int | Id продавца
$patch_delivery_kpi_ct_request = new \Ensi\LogisticClient\Dto\PatchDeliveryKpiCtRequest(); // \Ensi\LogisticClient\Dto\PatchDeliveryKpiCtRequest | 

try {
    $result = $apiInstance->patchDeliveryKpiCt($seller_id, $patch_delivery_kpi_ct_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KpiApi->patchDeliveryKpiCt: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seller_id** | **int**| Id продавца |
 **patch_delivery_kpi_ct_request** | [**\Ensi\LogisticClient\Dto\PatchDeliveryKpiCtRequest**](../Model/PatchDeliveryKpiCtRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryKpiCtResponse**](../Model/DeliveryKpiCtResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDeliveryKpiPpt

> \Ensi\LogisticClient\Dto\DeliveryKpiPptResponse patchDeliveryKpiPpt($seller_id, $patch_delivery_kpi_ppt_request)

Замена объекта типа DeliveryKpiPpt

Замена объекта типа DeliveryKpiPpt

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\KpiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$seller_id = 1; // int | Id продавца
$patch_delivery_kpi_ppt_request = new \Ensi\LogisticClient\Dto\PatchDeliveryKpiPptRequest(); // \Ensi\LogisticClient\Dto\PatchDeliveryKpiPptRequest | 

try {
    $result = $apiInstance->patchDeliveryKpiPpt($seller_id, $patch_delivery_kpi_ppt_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KpiApi->patchDeliveryKpiPpt: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **seller_id** | **int**| Id продавца |
 **patch_delivery_kpi_ppt_request** | [**\Ensi\LogisticClient\Dto\PatchDeliveryKpiPptRequest**](../Model/PatchDeliveryKpiPptRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryKpiPptResponse**](../Model/DeliveryKpiPptResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDeliveryKpiCts

> \Ensi\LogisticClient\Dto\SearchDeliveryKpiCtResponse searchDeliveryKpiCts($search_delivery_kpi_ct_request)

Поиск объектов типа DeliveryKpiCt

Поиск объектов типа DeliveryKpiCt

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\KpiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_delivery_kpi_ct_request = new \Ensi\LogisticClient\Dto\SearchDeliveryKpiCtRequest(); // \Ensi\LogisticClient\Dto\SearchDeliveryKpiCtRequest | 

try {
    $result = $apiInstance->searchDeliveryKpiCts($search_delivery_kpi_ct_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KpiApi->searchDeliveryKpiCts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_delivery_kpi_ct_request** | [**\Ensi\LogisticClient\Dto\SearchDeliveryKpiCtRequest**](../Model/SearchDeliveryKpiCtRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\SearchDeliveryKpiCtResponse**](../Model/SearchDeliveryKpiCtResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDeliveryKpiPpts

> \Ensi\LogisticClient\Dto\SearchDeliveryKpiPptResponse searchDeliveryKpiPpts($search_delivery_kpi_ppt_request)

Поиск объектов типа DeliveryKpiPpt

Поиск объектов типа DeliveryKpiPpt

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\KpiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_delivery_kpi_ppt_request = new \Ensi\LogisticClient\Dto\SearchDeliveryKpiPptRequest(); // \Ensi\LogisticClient\Dto\SearchDeliveryKpiPptRequest | 

try {
    $result = $apiInstance->searchDeliveryKpiPpts($search_delivery_kpi_ppt_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KpiApi->searchDeliveryKpiPpts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_delivery_kpi_ppt_request** | [**\Ensi\LogisticClient\Dto\SearchDeliveryKpiPptRequest**](../Model/SearchDeliveryKpiPptRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\SearchDeliveryKpiPptResponse**](../Model/SearchDeliveryKpiPptResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOneDeliveryKpiCt

> \Ensi\LogisticClient\Dto\DeliveryKpiCtResponse searchOneDeliveryKpiCt($search_delivery_kpi_ct_request)

Поиск объекта типа DeliveryKpiCt

Поиск объектов типа DeliveryKpiCt

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\KpiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_delivery_kpi_ct_request = new \Ensi\LogisticClient\Dto\SearchDeliveryKpiCtRequest(); // \Ensi\LogisticClient\Dto\SearchDeliveryKpiCtRequest | 

try {
    $result = $apiInstance->searchOneDeliveryKpiCt($search_delivery_kpi_ct_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KpiApi->searchOneDeliveryKpiCt: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_delivery_kpi_ct_request** | [**\Ensi\LogisticClient\Dto\SearchDeliveryKpiCtRequest**](../Model/SearchDeliveryKpiCtRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryKpiCtResponse**](../Model/DeliveryKpiCtResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOneDeliveryKpiPpt

> \Ensi\LogisticClient\Dto\DeliveryKpiPptResponse searchOneDeliveryKpiPpt($search_delivery_kpi_ppt_request)

Поиск объекта типа DeliveryKpiPpt

Поиск объектов типа DeliveryKpiPpt

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\KpiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_delivery_kpi_ppt_request = new \Ensi\LogisticClient\Dto\SearchDeliveryKpiPptRequest(); // \Ensi\LogisticClient\Dto\SearchDeliveryKpiPptRequest | 

try {
    $result = $apiInstance->searchOneDeliveryKpiPpt($search_delivery_kpi_ppt_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KpiApi->searchOneDeliveryKpiPpt: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_delivery_kpi_ppt_request** | [**\Ensi\LogisticClient\Dto\SearchDeliveryKpiPptRequest**](../Model/SearchDeliveryKpiPptRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryKpiPptResponse**](../Model/DeliveryKpiPptResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

