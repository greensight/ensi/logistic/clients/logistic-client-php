# Ensi\LogisticClient\DeliveryOrdersApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDeliveryOrderStatusMapping**](DeliveryOrdersApi.md#createDeliveryOrderStatusMapping) | **POST** /delivery-orders/delivery-order-status-mapping | Создание объекта типа DeliveryOrderStatusMapping
[**deleteDeliveryOrderStatusMapping**](DeliveryOrdersApi.md#deleteDeliveryOrderStatusMapping) | **DELETE** /delivery-orders/delivery-order-status-mapping/{id} | Удаление объекта типа DeliveryOrderStatusMapping
[**getDeliveryOrderStatusMapping**](DeliveryOrdersApi.md#getDeliveryOrderStatusMapping) | **GET** /delivery-orders/delivery-order-status-mapping/{id} | Получение объекта типа DeliveryOrderStatusMapping
[**patchDeliveryOrder**](DeliveryOrdersApi.md#patchDeliveryOrder) | **PATCH** /delivery-orders/delivery-orders/{id} | Обновление части полей объекта типа DeliveryOrder
[**patchDeliveryOrderStatusMapping**](DeliveryOrdersApi.md#patchDeliveryOrderStatusMapping) | **PATCH** /delivery-orders/delivery-order-status-mapping/{id} | Обновление объекта типа DeliveryOrderStatusMapping
[**searchDeliveryOrder**](DeliveryOrdersApi.md#searchDeliveryOrder) | **POST** /delivery-orders/delivery-orders:search-one | Поиск объекта типа DeliveryOrders
[**searchDeliveryOrderStatusMapping**](DeliveryOrdersApi.md#searchDeliveryOrderStatusMapping) | **POST** /delivery-orders/delivery-order-status-mapping:search | Поиск объектов типа DeliveryOrderStatusMapping
[**searchDeliveryOrders**](DeliveryOrdersApi.md#searchDeliveryOrders) | **POST** /delivery-orders/delivery-orders:search | Поиск объектов типа DeliveryOrders
[**searchOneDeliveryOrderStatusMapping**](DeliveryOrdersApi.md#searchOneDeliveryOrderStatusMapping) | **POST** /delivery-orders/delivery-order-status-mapping:search-one | Поиск объекта типа DeliveryOrderStatusMapping



## createDeliveryOrderStatusMapping

> \Ensi\LogisticClient\Dto\DeliveryOrderStatusMappingResponse createDeliveryOrderStatusMapping($create_delivery_order_status_mapping_request)

Создание объекта типа DeliveryOrderStatusMapping

Создание объекта типа DeliveryOrderStatusMapping

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryOrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_delivery_order_status_mapping_request = new \Ensi\LogisticClient\Dto\CreateDeliveryOrderStatusMappingRequest(); // \Ensi\LogisticClient\Dto\CreateDeliveryOrderStatusMappingRequest | 

try {
    $result = $apiInstance->createDeliveryOrderStatusMapping($create_delivery_order_status_mapping_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryOrdersApi->createDeliveryOrderStatusMapping: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_delivery_order_status_mapping_request** | [**\Ensi\LogisticClient\Dto\CreateDeliveryOrderStatusMappingRequest**](../Model/CreateDeliveryOrderStatusMappingRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryOrderStatusMappingResponse**](../Model/DeliveryOrderStatusMappingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteDeliveryOrderStatusMapping

> \Ensi\LogisticClient\Dto\EmptyDataResponse deleteDeliveryOrderStatusMapping($id)

Удаление объекта типа DeliveryOrderStatusMapping

Удаление объекта типа DeliveryOrderStatusMapping

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryOrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteDeliveryOrderStatusMapping($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryOrdersApi->deleteDeliveryOrderStatusMapping: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\LogisticClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDeliveryOrderStatusMapping

> \Ensi\LogisticClient\Dto\DeliveryOrderStatusMappingResponse getDeliveryOrderStatusMapping($id, $include)

Получение объекта типа DeliveryOrderStatusMapping

Получение объекта типа DeliveryOrderStatusMapping

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryOrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getDeliveryOrderStatusMapping($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryOrdersApi->getDeliveryOrderStatusMapping: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryOrderStatusMappingResponse**](../Model/DeliveryOrderStatusMappingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDeliveryOrder

> \Ensi\LogisticClient\Dto\DeliveryOrderResponse patchDeliveryOrder($id, $patch_delivery_order_request)

Обновление части полей объекта типа DeliveryOrder

Обновление части полей объекта типа DeliveryOrder

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryOrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_delivery_order_request = new \Ensi\LogisticClient\Dto\PatchDeliveryOrderRequest(); // \Ensi\LogisticClient\Dto\PatchDeliveryOrderRequest | 

try {
    $result = $apiInstance->patchDeliveryOrder($id, $patch_delivery_order_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryOrdersApi->patchDeliveryOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_delivery_order_request** | [**\Ensi\LogisticClient\Dto\PatchDeliveryOrderRequest**](../Model/PatchDeliveryOrderRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryOrderResponse**](../Model/DeliveryOrderResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDeliveryOrderStatusMapping

> \Ensi\LogisticClient\Dto\DeliveryOrderStatusMappingResponse patchDeliveryOrderStatusMapping($id, $patch_delivery_order_status_mapping_request)

Обновление объекта типа DeliveryOrderStatusMapping

Обновление объекта типа DeliveryOrderStatusMapping

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryOrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_delivery_order_status_mapping_request = new \Ensi\LogisticClient\Dto\PatchDeliveryOrderStatusMappingRequest(); // \Ensi\LogisticClient\Dto\PatchDeliveryOrderStatusMappingRequest | 

try {
    $result = $apiInstance->patchDeliveryOrderStatusMapping($id, $patch_delivery_order_status_mapping_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryOrdersApi->patchDeliveryOrderStatusMapping: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_delivery_order_status_mapping_request** | [**\Ensi\LogisticClient\Dto\PatchDeliveryOrderStatusMappingRequest**](../Model/PatchDeliveryOrderStatusMappingRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryOrderStatusMappingResponse**](../Model/DeliveryOrderStatusMappingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDeliveryOrder

> \Ensi\LogisticClient\Dto\DeliveryOrderResponse searchDeliveryOrder($search_delivery_orders_request)

Поиск объекта типа DeliveryOrders

Поиск объектов типа DeliveryOrders

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryOrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_delivery_orders_request = new \Ensi\LogisticClient\Dto\SearchDeliveryOrdersRequest(); // \Ensi\LogisticClient\Dto\SearchDeliveryOrdersRequest | 

try {
    $result = $apiInstance->searchDeliveryOrder($search_delivery_orders_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryOrdersApi->searchDeliveryOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_delivery_orders_request** | [**\Ensi\LogisticClient\Dto\SearchDeliveryOrdersRequest**](../Model/SearchDeliveryOrdersRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryOrderResponse**](../Model/DeliveryOrderResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDeliveryOrderStatusMapping

> \Ensi\LogisticClient\Dto\SearchDeliveryOrderStatusMappingResponse searchDeliveryOrderStatusMapping($search_delivery_order_status_mapping_request)

Поиск объектов типа DeliveryOrderStatusMapping

Поиск объектов типа DeliveryOrderStatusMapping

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryOrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_delivery_order_status_mapping_request = new \Ensi\LogisticClient\Dto\SearchDeliveryOrderStatusMappingRequest(); // \Ensi\LogisticClient\Dto\SearchDeliveryOrderStatusMappingRequest | 

try {
    $result = $apiInstance->searchDeliveryOrderStatusMapping($search_delivery_order_status_mapping_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryOrdersApi->searchDeliveryOrderStatusMapping: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_delivery_order_status_mapping_request** | [**\Ensi\LogisticClient\Dto\SearchDeliveryOrderStatusMappingRequest**](../Model/SearchDeliveryOrderStatusMappingRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\SearchDeliveryOrderStatusMappingResponse**](../Model/SearchDeliveryOrderStatusMappingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDeliveryOrders

> \Ensi\LogisticClient\Dto\SearchDeliveryOrdersResponse searchDeliveryOrders($search_delivery_orders_request)

Поиск объектов типа DeliveryOrders

Поиск объектов типа DeliveryOrders

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryOrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_delivery_orders_request = new \Ensi\LogisticClient\Dto\SearchDeliveryOrdersRequest(); // \Ensi\LogisticClient\Dto\SearchDeliveryOrdersRequest | 

try {
    $result = $apiInstance->searchDeliveryOrders($search_delivery_orders_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryOrdersApi->searchDeliveryOrders: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_delivery_orders_request** | [**\Ensi\LogisticClient\Dto\SearchDeliveryOrdersRequest**](../Model/SearchDeliveryOrdersRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\SearchDeliveryOrdersResponse**](../Model/SearchDeliveryOrdersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOneDeliveryOrderStatusMapping

> \Ensi\LogisticClient\Dto\DeliveryOrderStatusMappingResponse searchOneDeliveryOrderStatusMapping($search_delivery_order_status_mapping_request)

Поиск объекта типа DeliveryOrderStatusMapping

Поиск объектов типа DeliveryOrderStatusMapping

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryOrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_delivery_order_status_mapping_request = new \Ensi\LogisticClient\Dto\SearchDeliveryOrderStatusMappingRequest(); // \Ensi\LogisticClient\Dto\SearchDeliveryOrderStatusMappingRequest | 

try {
    $result = $apiInstance->searchOneDeliveryOrderStatusMapping($search_delivery_order_status_mapping_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryOrdersApi->searchOneDeliveryOrderStatusMapping: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_delivery_order_status_mapping_request** | [**\Ensi\LogisticClient\Dto\SearchDeliveryOrderStatusMappingRequest**](../Model/SearchDeliveryOrderStatusMappingRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryOrderStatusMappingResponse**](../Model/DeliveryOrderStatusMappingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

