# Ensi\LogisticClient\GeosApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createFederalDistrict**](GeosApi.md#createFederalDistrict) | **POST** /geos/federal-districts | Создание объекта типа FederalDistrict
[**createRegion**](GeosApi.md#createRegion) | **POST** /geos/regions | Создание объекта типа Region
[**deleteFederalDistrict**](GeosApi.md#deleteFederalDistrict) | **DELETE** /geos/federal-districts/{id} | Удаление объекта типа FederalDistrict
[**deleteRegion**](GeosApi.md#deleteRegion) | **DELETE** /geos/regions/{id} | Удаление объекта типа Region
[**getFederalDistrict**](GeosApi.md#getFederalDistrict) | **GET** /geos/federal-districts/{id} | Получение объекта типа FederalDistrict
[**getPoint**](GeosApi.md#getPoint) | **GET** /geos/points/{id} | Получение объекта типа Point
[**getRegion**](GeosApi.md#getRegion) | **GET** /geos/regions/{id} | Получение объекта типа Region
[**patchFederalDistrict**](GeosApi.md#patchFederalDistrict) | **PATCH** /geos/federal-districts/{id} | Обновления части полей объекта типа FederalDistrict
[**patchPoint**](GeosApi.md#patchPoint) | **PATCH** /geos/points/{id} | Обновления части полей объекта типа Point
[**patchRegion**](GeosApi.md#patchRegion) | **PATCH** /geos/regions/{id} | Обновления части полей объекта типа Region
[**searchDeliveryCities**](GeosApi.md#searchDeliveryCities) | **POST** /geos/delivery-cities:search | Поиск связи городов со службами доставки
[**searchFederalDistrict**](GeosApi.md#searchFederalDistrict) | **POST** /geos/federal-districts:search-one | Поиск объекта типа FederalDistrict
[**searchFederalDistricts**](GeosApi.md#searchFederalDistricts) | **POST** /geos/federal-districts:search | Поиск объектов типа FederalDistrict
[**searchPoints**](GeosApi.md#searchPoints) | **POST** /geos/points:search | Поиск пунктов приема/выдачи товара
[**searchRegion**](GeosApi.md#searchRegion) | **POST** /geos/regions:search-one | Поиск объекта типа Region
[**searchRegions**](GeosApi.md#searchRegions) | **POST** /geos/regions:search | Поиск объектов типа Region



## createFederalDistrict

> \Ensi\LogisticClient\Dto\FederalDistrictResponse createFederalDistrict($create_federal_district_request)

Создание объекта типа FederalDistrict

Создание объекта типа FederalDistrict

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\GeosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_federal_district_request = new \Ensi\LogisticClient\Dto\CreateFederalDistrictRequest(); // \Ensi\LogisticClient\Dto\CreateFederalDistrictRequest | 

try {
    $result = $apiInstance->createFederalDistrict($create_federal_district_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeosApi->createFederalDistrict: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_federal_district_request** | [**\Ensi\LogisticClient\Dto\CreateFederalDistrictRequest**](../Model/CreateFederalDistrictRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\FederalDistrictResponse**](../Model/FederalDistrictResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createRegion

> \Ensi\LogisticClient\Dto\RegionResponse createRegion($create_region_request)

Создание объекта типа Region

Создание объекта типа Region

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\GeosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_region_request = new \Ensi\LogisticClient\Dto\CreateRegionRequest(); // \Ensi\LogisticClient\Dto\CreateRegionRequest | 

try {
    $result = $apiInstance->createRegion($create_region_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeosApi->createRegion: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_region_request** | [**\Ensi\LogisticClient\Dto\CreateRegionRequest**](../Model/CreateRegionRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\RegionResponse**](../Model/RegionResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteFederalDistrict

> \Ensi\LogisticClient\Dto\EmptyDataResponse deleteFederalDistrict($id)

Удаление объекта типа FederalDistrict

Удаление объекта типа FederalDistrict

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\GeosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteFederalDistrict($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeosApi->deleteFederalDistrict: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\LogisticClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteRegion

> \Ensi\LogisticClient\Dto\EmptyDataResponse deleteRegion($id)

Удаление объекта типа Region

Удаление объекта типа Region

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\GeosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteRegion($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeosApi->deleteRegion: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\LogisticClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getFederalDistrict

> \Ensi\LogisticClient\Dto\FederalDistrictResponse getFederalDistrict($id, $include)

Получение объекта типа FederalDistrict

Получение объекта типа FederalDistrict

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\GeosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getFederalDistrict($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeosApi->getFederalDistrict: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\LogisticClient\Dto\FederalDistrictResponse**](../Model/FederalDistrictResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPoint

> \Ensi\LogisticClient\Dto\PointResponse getPoint($id, $include)

Получение объекта типа Point

Получение объекта типа Point

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\GeosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getPoint($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeosApi->getPoint: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\LogisticClient\Dto\PointResponse**](../Model/PointResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getRegion

> \Ensi\LogisticClient\Dto\RegionResponse getRegion($id, $include)

Получение объекта типа Region

Получение объекта типа Region

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\GeosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getRegion($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeosApi->getRegion: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\LogisticClient\Dto\RegionResponse**](../Model/RegionResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchFederalDistrict

> \Ensi\LogisticClient\Dto\FederalDistrictResponse patchFederalDistrict($id, $patch_federal_district_request)

Обновления части полей объекта типа FederalDistrict

Обновления части полей объекта типа FederalDistrict

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\GeosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_federal_district_request = new \Ensi\LogisticClient\Dto\PatchFederalDistrictRequest(); // \Ensi\LogisticClient\Dto\PatchFederalDistrictRequest | 

try {
    $result = $apiInstance->patchFederalDistrict($id, $patch_federal_district_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeosApi->patchFederalDistrict: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_federal_district_request** | [**\Ensi\LogisticClient\Dto\PatchFederalDistrictRequest**](../Model/PatchFederalDistrictRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\FederalDistrictResponse**](../Model/FederalDistrictResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchPoint

> \Ensi\LogisticClient\Dto\PointResponse patchPoint($id, $patch_point_request)

Обновления части полей объекта типа Point

Обновления части полей объекта типа Point

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\GeosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_point_request = new \Ensi\LogisticClient\Dto\PatchPointRequest(); // \Ensi\LogisticClient\Dto\PatchPointRequest | 

try {
    $result = $apiInstance->patchPoint($id, $patch_point_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeosApi->patchPoint: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_point_request** | [**\Ensi\LogisticClient\Dto\PatchPointRequest**](../Model/PatchPointRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\PointResponse**](../Model/PointResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchRegion

> \Ensi\LogisticClient\Dto\RegionResponse patchRegion($id, $patch_region_request)

Обновления части полей объекта типа Region

Обновления части полей объекта типа Region

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\GeosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_region_request = new \Ensi\LogisticClient\Dto\PatchRegionRequest(); // \Ensi\LogisticClient\Dto\PatchRegionRequest | 

try {
    $result = $apiInstance->patchRegion($id, $patch_region_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeosApi->patchRegion: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_region_request** | [**\Ensi\LogisticClient\Dto\PatchRegionRequest**](../Model/PatchRegionRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\RegionResponse**](../Model/RegionResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDeliveryCities

> \Ensi\LogisticClient\Dto\SearchDeliveryCitiesResponse searchDeliveryCities($search_delivery_cities_request)

Поиск связи городов со службами доставки

Поиск связи городов со службами доставки

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\GeosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_delivery_cities_request = new \Ensi\LogisticClient\Dto\SearchDeliveryCitiesRequest(); // \Ensi\LogisticClient\Dto\SearchDeliveryCitiesRequest | 

try {
    $result = $apiInstance->searchDeliveryCities($search_delivery_cities_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeosApi->searchDeliveryCities: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_delivery_cities_request** | [**\Ensi\LogisticClient\Dto\SearchDeliveryCitiesRequest**](../Model/SearchDeliveryCitiesRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\SearchDeliveryCitiesResponse**](../Model/SearchDeliveryCitiesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchFederalDistrict

> \Ensi\LogisticClient\Dto\FederalDistrictResponse searchFederalDistrict($search_federal_districts_request)

Поиск объекта типа FederalDistrict

Поиск объектов типа FederalDistrict

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\GeosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_federal_districts_request = new \Ensi\LogisticClient\Dto\SearchFederalDistrictsRequest(); // \Ensi\LogisticClient\Dto\SearchFederalDistrictsRequest | 

try {
    $result = $apiInstance->searchFederalDistrict($search_federal_districts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeosApi->searchFederalDistrict: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_federal_districts_request** | [**\Ensi\LogisticClient\Dto\SearchFederalDistrictsRequest**](../Model/SearchFederalDistrictsRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\FederalDistrictResponse**](../Model/FederalDistrictResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchFederalDistricts

> \Ensi\LogisticClient\Dto\SearchFederalDistrictsResponse searchFederalDistricts($search_federal_districts_request)

Поиск объектов типа FederalDistrict

Поиск объектов типа FederalDistrict

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\GeosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_federal_districts_request = new \Ensi\LogisticClient\Dto\SearchFederalDistrictsRequest(); // \Ensi\LogisticClient\Dto\SearchFederalDistrictsRequest | 

try {
    $result = $apiInstance->searchFederalDistricts($search_federal_districts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeosApi->searchFederalDistricts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_federal_districts_request** | [**\Ensi\LogisticClient\Dto\SearchFederalDistrictsRequest**](../Model/SearchFederalDistrictsRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\SearchFederalDistrictsResponse**](../Model/SearchFederalDistrictsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchPoints

> \Ensi\LogisticClient\Dto\SearchPointsResponse searchPoints($search_points_request)

Поиск пунктов приема/выдачи товара

Поиск пунктов приема/выдачи товара

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\GeosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_points_request = new \Ensi\LogisticClient\Dto\SearchPointsRequest(); // \Ensi\LogisticClient\Dto\SearchPointsRequest | 

try {
    $result = $apiInstance->searchPoints($search_points_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeosApi->searchPoints: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_points_request** | [**\Ensi\LogisticClient\Dto\SearchPointsRequest**](../Model/SearchPointsRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\SearchPointsResponse**](../Model/SearchPointsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchRegion

> \Ensi\LogisticClient\Dto\RegionResponse searchRegion($search_regions_request)

Поиск объекта типа Region

Поиск объектов типа Region

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\GeosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_regions_request = new \Ensi\LogisticClient\Dto\SearchRegionsRequest(); // \Ensi\LogisticClient\Dto\SearchRegionsRequest | 

try {
    $result = $apiInstance->searchRegion($search_regions_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeosApi->searchRegion: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_regions_request** | [**\Ensi\LogisticClient\Dto\SearchRegionsRequest**](../Model/SearchRegionsRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\RegionResponse**](../Model/RegionResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchRegions

> \Ensi\LogisticClient\Dto\SearchRegionsResponse searchRegions($search_regions_request)

Поиск объектов типа Region

Поиск объектов типа Region

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\GeosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_regions_request = new \Ensi\LogisticClient\Dto\SearchRegionsRequest(); // \Ensi\LogisticClient\Dto\SearchRegionsRequest | 

try {
    $result = $apiInstance->searchRegions($search_regions_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeosApi->searchRegions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_regions_request** | [**\Ensi\LogisticClient\Dto\SearchRegionsRequest**](../Model/SearchRegionsRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\SearchRegionsResponse**](../Model/SearchRegionsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

