# Ensi\LogisticClient\DeliveryPricesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkoutDeliveryPrices**](DeliveryPricesApi.md#checkoutDeliveryPrices) | **POST** /delivery-prices/delivery-prices:checkout | Поиск объектов типа DeliveryPrice для чекаута
[**createDeliveryPrice**](DeliveryPricesApi.md#createDeliveryPrice) | **POST** /delivery-prices/delivery-prices | Создание объекта типа DeliveryPrice
[**deleteDeliveryPrice**](DeliveryPricesApi.md#deleteDeliveryPrice) | **DELETE** /delivery-prices/delivery-prices/{id} | Удаление объекта типа DeliveryPrice
[**getDeliveryPrice**](DeliveryPricesApi.md#getDeliveryPrice) | **GET** /delivery-prices/delivery-prices/{id} | Получение объекта типа DeliveryPrice
[**patchDeliveryPrice**](DeliveryPricesApi.md#patchDeliveryPrice) | **PATCH** /delivery-prices/delivery-prices/{id} | Обновления части полей объекта типа DeliveryPrice
[**searchDeliveryPrice**](DeliveryPricesApi.md#searchDeliveryPrice) | **POST** /delivery-prices/delivery-prices:search-one | Поиск объекта типа DeliveryPrice
[**searchDeliveryPrices**](DeliveryPricesApi.md#searchDeliveryPrices) | **POST** /delivery-prices/delivery-prices:search | Поиск объектов типа DeliveryPrice



## checkoutDeliveryPrices

> \Ensi\LogisticClient\Dto\CheckoutDeliveryPriceResponse checkoutDeliveryPrices($checkout_delivery_price_request)

Поиск объектов типа DeliveryPrice для чекаута

Поиск объектов типа DeliveryPrice для чекаута

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryPricesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$checkout_delivery_price_request = new \Ensi\LogisticClient\Dto\CheckoutDeliveryPriceRequest(); // \Ensi\LogisticClient\Dto\CheckoutDeliveryPriceRequest | 

try {
    $result = $apiInstance->checkoutDeliveryPrices($checkout_delivery_price_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryPricesApi->checkoutDeliveryPrices: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **checkout_delivery_price_request** | [**\Ensi\LogisticClient\Dto\CheckoutDeliveryPriceRequest**](../Model/CheckoutDeliveryPriceRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\CheckoutDeliveryPriceResponse**](../Model/CheckoutDeliveryPriceResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createDeliveryPrice

> \Ensi\LogisticClient\Dto\DeliveryPriceResponse createDeliveryPrice($create_delivery_price_request)

Создание объекта типа DeliveryPrice

Создание объекта типа DeliveryPrice

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryPricesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_delivery_price_request = new \Ensi\LogisticClient\Dto\CreateDeliveryPriceRequest(); // \Ensi\LogisticClient\Dto\CreateDeliveryPriceRequest | 

try {
    $result = $apiInstance->createDeliveryPrice($create_delivery_price_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryPricesApi->createDeliveryPrice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_delivery_price_request** | [**\Ensi\LogisticClient\Dto\CreateDeliveryPriceRequest**](../Model/CreateDeliveryPriceRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryPriceResponse**](../Model/DeliveryPriceResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteDeliveryPrice

> \Ensi\LogisticClient\Dto\EmptyDataResponse deleteDeliveryPrice($id)

Удаление объекта типа DeliveryPrice

Удаление объекта типа DeliveryPrice

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryPricesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteDeliveryPrice($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryPricesApi->deleteDeliveryPrice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\LogisticClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDeliveryPrice

> \Ensi\LogisticClient\Dto\DeliveryPriceResponse getDeliveryPrice($id, $include)

Получение объекта типа DeliveryPrice

Получение объекта типа DeliveryPrice

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryPricesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getDeliveryPrice($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryPricesApi->getDeliveryPrice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryPriceResponse**](../Model/DeliveryPriceResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDeliveryPrice

> \Ensi\LogisticClient\Dto\DeliveryPriceResponse patchDeliveryPrice($id, $patch_delivery_price_request)

Обновления части полей объекта типа DeliveryPrice

Обновления части полей объекта типа DeliveryPrice

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryPricesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_delivery_price_request = new \Ensi\LogisticClient\Dto\PatchDeliveryPriceRequest(); // \Ensi\LogisticClient\Dto\PatchDeliveryPriceRequest | 

try {
    $result = $apiInstance->patchDeliveryPrice($id, $patch_delivery_price_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryPricesApi->patchDeliveryPrice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_delivery_price_request** | [**\Ensi\LogisticClient\Dto\PatchDeliveryPriceRequest**](../Model/PatchDeliveryPriceRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryPriceResponse**](../Model/DeliveryPriceResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDeliveryPrice

> \Ensi\LogisticClient\Dto\DeliveryPriceResponse searchDeliveryPrice($search_delivery_prices_request)

Поиск объекта типа DeliveryPrice

Поиск объектов типа DeliveryPrice

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryPricesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_delivery_prices_request = new \Ensi\LogisticClient\Dto\SearchDeliveryPricesRequest(); // \Ensi\LogisticClient\Dto\SearchDeliveryPricesRequest | 

try {
    $result = $apiInstance->searchDeliveryPrice($search_delivery_prices_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryPricesApi->searchDeliveryPrice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_delivery_prices_request** | [**\Ensi\LogisticClient\Dto\SearchDeliveryPricesRequest**](../Model/SearchDeliveryPricesRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\DeliveryPriceResponse**](../Model/DeliveryPriceResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDeliveryPrices

> \Ensi\LogisticClient\Dto\SearchDeliveryPricesResponse searchDeliveryPrices($search_delivery_prices_request)

Поиск объектов типа DeliveryPrice

Поиск объектов типа DeliveryPrice

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\LogisticClient\Api\DeliveryPricesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_delivery_prices_request = new \Ensi\LogisticClient\Dto\SearchDeliveryPricesRequest(); // \Ensi\LogisticClient\Dto\SearchDeliveryPricesRequest | 

try {
    $result = $apiInstance->searchDeliveryPrices($search_delivery_prices_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryPricesApi->searchDeliveryPrices: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_delivery_prices_request** | [**\Ensi\LogisticClient\Dto\SearchDeliveryPricesRequest**](../Model/SearchDeliveryPricesRequest.md)|  |

### Return type

[**\Ensi\LogisticClient\Dto\SearchDeliveryPricesResponse**](../Model/SearchDeliveryPricesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

